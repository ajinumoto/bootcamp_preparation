import java.util.Locale;


abstract class Worker {
    int id;
    String nama;
    long gajiPokok;
    String karyawan;
    int hariKerja;
    long akomodasi;
    long x;
    
    public Worker(int id, String nama, long gajiPokok, String karyawan, int hariKerja, long x, long akomodasi){
        this.id = id;
        this.nama = nama;
        this.gajiPokok = gajiPokok;
        this.karyawan = karyawan;
        this.hariKerja = hariKerja;
        this.x = x;
        this.akomodasi = akomodasi;
    }

    public long tunjanganAkomodasi() {
        return (hariKerja*akomodasi);
    }

    public abstract long totalGaji();
}

class Staff extends Worker {

    public Staff(int id, String nama, long gajiPokok, String karyawan, int hariKerja, long x, long akomodasi) {
        super(id, nama, gajiPokok, karyawan, hariKerja, x, akomodasi);
    }

    public long lemburGaji() {
        long lemburPerJam = 10000;
        return (super.x*lemburPerJam);
    }

    @Override
    public long totalGaji() {
        return (super.gajiPokok + lemburGaji() + super.tunjanganAkomodasi());
    }
}

class Manager extends Worker {

    public Manager(int id, String nama, long gajiPokok, String karyawan, int hariKerja, long x, long akomodasi) {
        super(id, nama, gajiPokok, karyawan, hariKerja, x, akomodasi);
    }

    @Override
    public long totalGaji() {
        long gajiBonus = super.x;
        return (super.gajiPokok + gajiBonus + super.tunjanganAkomodasi());
    }
}


public class Laporan {

    public static void printKaryawan(Staff a) {
        System.out.println("-----------------------------------------------");
        System.out.println("ID              : " + a.id);
		System.out.println("Nama            : " + a.nama);
		System.out.printf("  Gaji Pokok    : Rp. %,d,00\n", a.gajiPokok);
		System.out.printf("  Akomodasi     : Rp.  %,d,00\n", a.tunjanganAkomodasi());
		System.out.printf("  Gaji Lembur   : Rp.  %,d,00\n", a.lemburGaji());
		System.out.printf("Total Gaji      : Rp.  %,d,00\n", a.totalGaji());
        System.out.println("-----------------------------------------------");
    }
    public static void printKaryawan(Manager a) {
        System.out.println("-----------------------------------------------");
        System.out.println("ID              : " + a.id);
		System.out.println("Nama            : " + a.nama);
		System.out.printf("  Gaji Pokok    : Rp. %,d,00\n", a.gajiPokok);
		System.out.printf("  Akomodasi     : Rp.  %,d,00\n", a.tunjanganAkomodasi());
		System.out.printf("  Gaji Bonus    : Rp.  %,d,00\n", a.x);
		System.out.printf("Total Gaji      : Rp.  %,d,00\n", a.totalGaji());
        System.out.println("-----------------------------------------------");
    }
    public static void main(String[] args) {  
        Locale.setDefault(new Locale("id", "ID"));
      
        Staff a = new Staff(1, "John", 4500000, "Staff", 19, 30 , 25000);
        Staff b = new Staff(2, "Peter", 5000000, "Staff", 10, 50 , 25000);
        Manager c = new Manager(3, "Linda", 10000000, "Manager", 15, 250000, 50000);
        Manager d = new Manager(4, "Lucy", 7000000, "Manager", 20, 500000, 50000);
        printKaryawan(a);
        printKaryawan(b);
        printKaryawan(c);
        printKaryawan(d);
    }
}
