import java.text.DecimalFormat;


interface Karakter {
    public void nama(int x);

    public void warna(int y);
}

public class Benda implements Karakter {

    private static final DecimalFormat df = new DecimalFormat("0.00");


    public void nama(int x) {
        switch (x) {
            case 1:
                System.out.println("Nama    : KUBUS");
                break;
            case 2:
                System.out.println("Nama    : KERUCUT");
                break;
            case 3:
                System.out.println("Nama    : BOLA");
                break;
            default:
                break;
        }
    }

    public void warna(int x) {
        switch (x) {
            case 1:
                System.out.println("Warna    : Merah");
                break;
            case 2:
                System.out.println("Warna    : Hijau");
                break;
            case 3:
                System.out.println("Warna    : Magenta");
                break;
            default:
                break;
        }
    }

    static void volume(int x) {
        Benda benda = new Benda();
        benda.nama(1);
        benda.warna(1);

        int z = x * x * x;

        System.out.println("Sisi    : " + x);
        System.out.println("V   : " + z);
    }

    static void volume(int x, int y) {
        Benda benda = new Benda();
        benda.nama(2);
        benda.warna(2);

        double z = Math.PI * x * x * y / 3.0;

        System.out.println("r   : " + x);
        System.out.println("t   : " + y);
        System.out.println("V   : " + Math.round(z));
    }

    static void volume(double x) {
        Benda benda = new Benda();
        benda.nama(3);
        benda.warna(3);

        double z = (4.0 / 3.0) * Math.PI * x * x * x;

        System.out.println("r   : " + x);
        System.out.println("V   : " + df.format(z));

    }

    public static void main(String[] args) {
        int a = 10;
        int b = 7;
        int c = 3;
        double d = 3.5;
        volume(a);
        System.out.println();
        volume(b, c);
        System.out.println();
        volume(d);
    }
}
